import asyncio
import websockets
import sys
import pickle
import inspect
import netifaces


def run_server(input_data, callback, server_port: int, listen_ip: str):
    data = iter(input_data())
    exhausted = [False]

    async def echo(websocket):
        # async for message in websocket:
        try:
            while True:
                g = next(data)
                g_serialized = pickle.dumps(g)
                await websocket.send(g_serialized)
                result_serialized = await websocket.recv()
                result = pickle.loads(result_serialized)
                callback(result)
        except StopIteration:
            if not exhausted[0]:
                print("EXHAUSTED")
            exhausted[0] = True

    async def main():
        async with websockets.serve(echo, listen_ip, server_port):
            # await asyncio.Future()  # run forever
            while not exhausted[0]:
                # print("hey")
                await asyncio.sleep(0.01)

    asyncio.run(main())


def run_client(func, server: str, port: int, verbose: bool):
    async def hello(uri):
        try:
            async with websockets.connect(uri) as websocket:
                # await websocket.send("mes infos")
                while True:
                    order_serialized = await websocket.recv()
                    order = pickle.loads(order_serialized)
                    if verbose: print(f"input: {order}")
                    result = func(order)
                    if verbose: print(f"result: {result}")
                    result_serialized = pickle.dumps(result)
                    await websocket.send(result_serialized)
        except websockets.exceptions.ConnectionClosedOK as ex:
            print("stop")

    asyncio.run(hello(f"ws://{server}:{port}"))


def guess_ip():
    ips = set([netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr'] for iface in netifaces.interfaces() if
               netifaces.AF_INET in netifaces.ifaddresses(iface)])
    try:
        ips.remove("0.0.0.0")
    except:
        pass
    try:
        ips.remove("127.0.0.1")
    except:
        pass
    ips = set(ip for ip in ips if ip.count(".") == 3)
    ips = list(ips)
    ips.sort(key=lambda x: x[::-1], reverse=True)
    if len(ips) > 0:
        return ips[0]
    else:
        return None


def distribute(function,  # function
               data,  # function
               results,  # function again
               *,  # (i love functions)
               client_verbose: bool = True,
               server_port: int = 43254,
               listen_ip="0.0.0.0",
               server_welcome: bool = True):
    if len(sys.argv) == 4 and sys.argv[1] == "connect":
        print("EpicPython client")
        server = sys.argv[2]
        port = int(sys.argv[3])
        run_client(function, server, port, client_verbose)
    else:
        if server_welcome:
            print("EpicPython server")
            filename = inspect.stack()[1].filename
            print(f"on client, run :")
            print(f"$ python3 {filename} connect {guess_ip() or '[server_ip]'} {server_port}")
            # print("(replace filename by the path to this script, and the IP with this PC IP if necessary)")
        run_server(data, results, server_port, listen_ip)
