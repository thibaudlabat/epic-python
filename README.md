# epic python

homemade distributed computing system for Python

# Demo
example.py
```python
import EpicPython
import math

def is_prime(n: int):
    # example computing function : is this number prime ?
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return n, False
    return n, True

def process_result(result: any):
    # called every time a computing result is received from a node
    i, i_is_prime = result
    if i_is_prime:
        print(i)

def input_data():
    # generates the iterable data sent, element by element, to the connected nodes
    print("PRIME NUMBERS")
    n = 10 ** 16 + 1
    return range(n, n + 100, 2)

EpicPython.distribute(function=is_prime, data=input_data, results=process_result)
```
![results](./Animation.gif)

# TODO
- Sérialisation avec Pickle/Marshall/JSON au choix (à l'utilisation)
- Pb de versions de Python différentes : set un range de versions acceptables (à l'utilisation)
-  setup pré-calcul côté client
- gestion de la déconnexion d'un client ? --> réallouer les données qu'il avait
- transmettre les erreurs
- Serveur : Ctrl+C --> enregistre l'état des données calculées dans un fichier pour reprendre plus tard (pickle)
- data item state: waiting, running, finished, error --> en faire 4 files
avec possibilité de relancer les items avec erreur
- gestion des clients : sauvegarder les données relatives à chaque connexion, et garder une trace des données pending de chaque client