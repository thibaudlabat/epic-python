#!/bin/bash
n_cores=$(grep -c ^processor /proc/cpuinfo)
echo $(hostname) $n_cores
n_run=$(bc <<< $n_cores/1)
for (( c=1; c <= $n_run; c++))
do
  python3 ./client.py > /dev/null &
done