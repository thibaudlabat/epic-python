import EpicPython
import math


def is_prime(n: int):
    # example computing function : is this number prime ?
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return n, False
    return n, True


def process_result(result: any):
    # called every time a computing result is received from a node
    i, i_is_prime = result
    if i_is_prime:
        print(i)


def input_data():
    # generates the iterable data sent, element by element, to the connected nodes
    print("PRIME NUMBERS")
    n = 10 ** 16 + 1
    return range(n, n + 100, 2)


EpicPython.distribute(function=is_prime, data=input_data, results=process_result)
